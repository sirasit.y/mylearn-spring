package th.co.login.enums;

public enum EnableStatus {
    DISABLE(0),
    ENABLE(1);

    private final int id;

    EnableStatus(int id) {
        this.id = id;
    }

    public int getValue() {
        return id;
    }

    public static EnableStatus as(String str) {
        for (EnableStatus me : EnableStatus.values()) {
            if (me.name().equalsIgnoreCase(str))
                return me;
        }
        return DISABLE;
    }
}
