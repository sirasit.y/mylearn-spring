package th.co.login.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;

@Service
public class SendMailService {

    @Autowired
    JavaMailSender javaMailSender;
    @Autowired
    FilesStorageService filesStorageService;

    public void sendEmail(String[] to, String[] cc, String subject, String content) throws MessagingException {
        MimeMessage msg = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(msg, true);
        helper.setTo(to);
        helper.setCc(cc);
        helper.setSubject(subject);
        helper.setText(content, true);
        javaMailSender.send(msg);
    }

    public void sendEmailWithAttachment(String[] to, String[] cc, String subject, String content, String path, String fileName) throws MessagingException, IOException {
        MimeMessage msg = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(msg, true);
        helper.setTo(to);
        helper.setCc(cc);
        helper.setSubject(subject);
        helper.setText(content, true);
        this.filesStorageService.init(path);
        Resource file = this.filesStorageService.load(fileName);
        helper.addAttachment(fileName, file.getFile());
        javaMailSender.send(msg);
    }
}
