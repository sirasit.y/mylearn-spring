package th.co.login.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import th.co.login.entity.UsersEntity;
import th.co.login.exception.ResourceNotFoundException;
import th.co.login.model.MemberModel;
import th.co.login.model.UsersModel;
import th.co.login.model.UsersRoleModel;
import th.co.login.model.message.StatusMessage;
import th.co.login.repository.UsersRepository;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UsersService {
    @Autowired
    UsersRepository usersRepository;

    @Transactional
    public StatusMessage create(UsersModel model) {
        Optional<UsersEntity> optionalUsers = this.usersRepository.findById(model.getUsername());
        if (optionalUsers.isPresent()) {
            UsersEntity entity = optionalUsers.get();
            if (!model.getPassword().equals(entity.getPassword())) {
                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
                entity.setPassword(passwordEncoder.encode(model.getPassword()));
            }
            entity.setMember(MemberModel.toEntity(model.getMember()));
            entity.setRole(UsersRoleModel.toEntity(model.getRole()));
            this.usersRepository.save(entity);
        } else {
            this.usersRepository.save(UsersModel.toEntity(model));
        }
        return StatusMessage.createInstanceSuccess("Data saved successfully.");
    }

    @Transactional
    public StatusMessage createTempPassword(String username, String tempPassword, Instant tempPasswordExpire) {
        this.usersRepository.findById(username)
                .ifPresent(e -> {
                    BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
                    e.setTempPassword(passwordEncoder.encode(tempPassword));
                    e.setTempPasswordExpire(tempPasswordExpire);
                    this.usersRepository.save(e);
                });
        return StatusMessage.createInstanceSuccess("Data saved successfully.");
    }

    @Transactional
    public StatusMessage changePassword(String username, String oldPassword, String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        Instant now = Instant.now();
        Optional<UsersEntity> optionalTempUsers = this.usersRepository.findByUsernameAndTempPasswordExpireAfter(username, now);
        if (optionalTempUsers.isPresent()) {
            UsersEntity tempUser = optionalTempUsers.get();
            boolean isMatches = passwordEncoder.matches(oldPassword, tempUser.getTempPassword());
            if (isMatches) {
                tempUser.setPassword(passwordEncoder.encode(password));
                this.usersRepository.save(tempUser);
                return StatusMessage.createInstanceSuccess("Change Password successfully.");
            }
        }

        Optional<UsersEntity> optionalUsers = this.usersRepository.findById(username);
        if (optionalUsers.isPresent()) {
            UsersEntity entity = optionalUsers.get();
            boolean isMatches = passwordEncoder.matches(oldPassword, entity.getPassword());
            if (isMatches) {
                entity.setPassword(passwordEncoder.encode(password));
                this.usersRepository.save(entity);
            } else {
                return StatusMessage.createInstanceFail("Password is incorrect");
            }
        }
        return StatusMessage.createInstanceSuccess("Change Password successfully.");
    }

    @Transactional
    public StatusMessage delete(UsersModel model) {
        this.usersRepository.findById(model.getUsername())
                .ifPresent(e -> this.usersRepository.delete(e));
        return StatusMessage.createInstanceSuccess("Data deleted successfully.");
    }

    @Transactional
    public List<UsersModel> findAllList() {
        return this.usersRepository.findAll(Sort.by(Sort.Direction.ASC, "username"))
                .stream()
                .map(UsersModel::fromEntity)
                .collect(Collectors.toList());
    }

    @Transactional
    public Page<UsersModel> findPage(PageRequest page) {
        return this.usersRepository.findAll(page.withSort(Sort.by(Sort.Direction.ASC, "username")))
                .map(UsersModel::fromEntity);
    }

    @Transactional
    public UsersModel findById(String username) {
        return this.usersRepository.findById(username)
                .map(UsersModel::fromEntity)
                .orElseThrow(() -> new ResourceNotFoundException("Data " + username + " not found"));
    }
}
