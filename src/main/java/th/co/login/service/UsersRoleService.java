package th.co.login.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import th.co.login.entity.UsersEntity;
import th.co.login.exception.ResourceNotFoundException;
import th.co.login.model.UsersRoleModel;
import th.co.login.model.message.StatusMessage;
import th.co.login.repository.UsersRepository;
import th.co.login.repository.UsersRoleRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UsersRoleService {
    @Autowired
    UsersRoleRepository roleRepository;
    @Autowired
    UsersRepository usersRepository;

    @Transactional
    public StatusMessage create(UsersRoleModel model) {
        this.roleRepository.save(UsersRoleModel.toEntity(model));
        return StatusMessage.createInstanceSuccess("Data saved successfully.");
    }

    @Transactional
    public StatusMessage delete(UsersRoleModel model) {
        this.roleRepository.findById(model.getRoleId())
                .ifPresent(e -> {
                    List<UsersEntity> list = this.usersRepository.findByRole(e)
                            .map(v -> {
                                v.setRole(null);
                                return v;
                            })
                            .collect(Collectors.toList());
                    this.usersRepository.saveAll(list);
                    this.roleRepository.delete(e);
                });
        return StatusMessage.createInstanceSuccess("Data deleted successfully.");
    }

    @Transactional
    public List<UsersRoleModel> findAllList() {
        return this.roleRepository.findAll(Sort.by(Sort.Direction.ASC, "roleId"))
                .stream()
                .map(UsersRoleModel::fromEntity)
                .collect(Collectors.toList());
    }

    @Transactional
    public Page<UsersRoleModel> findPage(PageRequest page) {
        return this.roleRepository.findAll(page.withSort(Sort.by(Sort.Direction.ASC, "roleId")))
                .map(UsersRoleModel::fromEntity);
    }

    @Transactional
    public UsersRoleModel findById(String roleId) {
        return this.roleRepository.findById(roleId)
                .map(UsersRoleModel::fromEntity)
                .orElseThrow(() -> new ResourceNotFoundException("Data " + roleId + " not found"));
    }
}
