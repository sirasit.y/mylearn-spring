package th.co.login.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.*;
import java.util.stream.Stream;

@Service
public class FilesStorageService {
    @Value("${upload.path:uploads}")
    private String uploadPath;
    private Path root = Paths.get("uploads");

    public void init(String pathUpload) {
        try {
            root = Paths.get(uploadPath);
            if (Files.notExists(root)) {
                Files.createDirectory(root);
            }
            root = Paths.get(uploadPath + pathUpload);
            if (Files.notExists(root)) {
                Files.createDirectory(root);
            }
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize folder for upload!");
        }
    }

    public void save(MultipartFile file, String newFileName) {
        try {
            Files.copy(file.getInputStream(), this.root.resolve(newFileName));
        } catch (Exception e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }
    }

    public void save(byte[] fileData, String newFileName) {
        try {
            Files.write(this.root.resolve(newFileName), fileData, StandardOpenOption.CREATE);
        } catch (Exception e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }
    }

    public Resource load(String filename) {
        try {
            Path file = root.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            return resource;
//            if (resource.exists() || resource.isReadable()) {
//                return resource;
//            } else {
//                throw new RuntimeException("Could not read the file!");
//            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    public void move(Resource source, String destinationPath) {
        try {
            Path destination = Paths.get(source.getFile().getParentFile() + "/" + destinationPath);
            if (Files.notExists(destination)) {
                Files.createDirectory(destination);
            }
            destination = Paths.get(source.getFile().getParentFile() + "/" + destinationPath + "/" + source.getFilename());
            Files.move(source.getFile().toPath(), destination, StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            throw new RuntimeException("Could not Move the file. Error: " + e.getMessage());
        }
    }

    public void delete(String pathName) {
        try {
            Files.delete(Paths.get(pathName));
        } catch (Exception e) {
            throw new RuntimeException("Could not delete the file. Error: " + e.getMessage());
        }
    }

    public void deleteAll() {
        FileSystemUtils.deleteRecursively(root.toFile());
    }

    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.root, 1).filter(path -> !path.equals(this.root)).map(this.root::relativize);
        } catch (IOException e) {
            throw new RuntimeException("Could not load the files!");
        }
    }
}
