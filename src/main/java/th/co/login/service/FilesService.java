package th.co.login.service;


import com.google.common.io.Files;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import th.co.login.model.message.StatusMessage;
import th.co.login.utilities.FilesName;

import javax.transaction.Transactional;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

@Service
public class FilesService {
    @Value("${image.extensions}")
    private String imageExtensions;
    @Value("${vdo.extensions}")
    private String vdoExtensions;
    @Value("${pdf.extensions}")
    private String pdfExtensions;

    @Autowired
    FilesStorageService filesStorageService;

    @Transactional
    public StatusMessage uploadImage(MultipartFile file) {
        this.filesStorageService.init("image");
        return this.saveFilesStorage(file, imageExtensions);
    }

    @Transactional
    public StatusMessage uploadVideo(MultipartFile file) {
        this.filesStorageService.init("video");
        return this.saveFilesStorage(file, vdoExtensions);
    }

    @Transactional
    public StatusMessage uploadPdf(MultipartFile file) {
        this.filesStorageService.init("pdf");
        return this.saveFilesStorage(file, pdfExtensions);
    }

    @Transactional
    public byte[] openImage(String filename) throws IOException {
        this.filesStorageService.init("image");
        Resource file = this.filesStorageService.load(filename);
        if (file.getFile().isFile()) {
            InputStream in = file.getInputStream();
            return IOUtils.toByteArray(in);
        } else {
            return new byte[]{};
        }
    }

    @Transactional
    public FileSystemResource openVideo(String filename) throws IOException {
        this.filesStorageService.init("video");
        Resource file = this.filesStorageService.load(filename);
        return new FileSystemResource(file.getFile());
    }

    @Transactional
    public FileSystemResource openPdf(String filename) throws IOException {
        this.filesStorageService.init("pdf");
        Resource file = this.filesStorageService.load(filename);
        return new FileSystemResource(file.getFile());
    }

    @Transactional
    public StatusMessage upload(MultipartFile file) {
        this.filesStorageService.init("file");
        UUID uuid = UUID.randomUUID();
        String filename = uuid + "." + FilesName.getFileExtension(file.getOriginalFilename());
        this.filesStorageService.save(file, filename);
        return StatusMessage.createInstanceSuccess("Data upload successfully.", filename);
    }

    @Transactional
    public FileSystemResource download(String filename) throws IOException {
        this.filesStorageService.init("file");
        Resource file = this.filesStorageService.load(filename);
        return new FileSystemResource(file.getFile());
    }

    private StatusMessage saveFilesStorage(MultipartFile file, String imageExtensions) {
        if (FilesName.getFileExtension(file.getOriginalFilename()).isEmpty()) {
            return StatusMessage.createInstanceFail("Invalid upload file");
        } else {
            String fileExtensions = FilesName.getFileExtension(file.getOriginalFilename());
            if (!imageExtensions.contains(fileExtensions)) {
                return StatusMessage.createInstanceFail("Please upload file : " + imageExtensions);
            }
        }
        UUID uuid = UUID.randomUUID();
        String filename = uuid + "." + FilesName.getFileExtension(file.getOriginalFilename());
        this.filesStorageService.save(file, filename);
        return StatusMessage.createInstanceSuccess("Data upload successfully.", filename);
    }
}
