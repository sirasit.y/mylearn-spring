package th.co.login.service;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import th.co.login.entity.MemberEntity;
import th.co.login.entity.UsersEntity;
import th.co.login.enums.EnableStatus;
import th.co.login.model.MemberModel;
import th.co.login.model.UsersModel;
import th.co.login.model.message.StatusMessage;
import th.co.login.model.request.RegisterRequestModel;
import th.co.login.model.response.LoginResponseModel;
import th.co.login.repository.MemberRepository;
import th.co.login.repository.UsersRepository;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import java.time.Instant;
import java.util.*;

@Service
public class AuthService {
//    @Autowired
//    AuthenticationManager authManager;
    @Autowired
    UsersService usersService;
    @Autowired
    SendMailService sendMailService;
    @Autowired
    UsersRepository usersRepository;
    @Autowired
    MemberRepository memberRepository;

    @Transactional
    public LoginResponseModel login(String username, String password) {
        try {
            UsersEntity usersEntity = null;
            Instant now = Instant.now();
            boolean isMatches = false;
            Optional<UsersEntity> optionalUsers = this.usersRepository.findByUsernameAndTempPasswordExpireAfter(username, now);
            if (optionalUsers.isPresent()) {
                UsersEntity tempUser = optionalUsers.get();
                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
                isMatches = passwordEncoder.matches(password, tempUser.getTempPassword());
                if (isMatches) {
                    usersEntity = tempUser;
                }
            }

//            if (!isMatches) {
//                Authentication authentication = authManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
//                usersEntity = (UsersEntity) authentication.getPrincipal();
//            }

            assert usersEntity != null;
            if (usersEntity.getMember().getStatus() == EnableStatus.ENABLE.getValue()) {
//                return this.genToken(UsersModel.fromEntity(usersEntity));
                return new LoginResponseModel(usersEntity.getUsername(), "", "");
            } else {
                throw new AuthenticationServiceException("Authentication UProfile not match");
            }
        } catch (BadCredentialsException ex) {
            throw new AuthenticationServiceException("Authentication UProfile not match");
        }
    }

//    @Transactional
//    public LoginResponseModel refreshToken(String oldRefreshToken) {
//        try {
//            UProfile refreshUp = this.refreshTokenService.parseToken(oldRefreshToken);
//            UsersModel usersModel = this.usersService.findById(refreshUp.getUsername());
//            return this.genToken(usersModel);
//        } catch (BadCredentialsException ex) {
//            throw new AuthenticationServiceException("Authentication UProfile not match");
//        }
//    }

//    @Transactional
//    public LoginResponseModel genToken(UsersModel usersModel) {
//        Map<String, String> claim = new HashMap<>();
//        List<String> roles = new ArrayList<>();
//        if (Optional.ofNullable(usersModel.getRole()).isPresent()) {
//            roles.add(usersModel.getRole().getRoleId());
//        }
//        UProfile up = new UProfile(usersModel.getUsername(), usersModel.getUsername(), usersModel.getUsername(), usersModel.getMember().getMemberId(), usersModel.getMember().getMemberId(), roles, claim);
//        String accessToken = this.accessTokenService.generateToken(up);
//        String refreshToken = this.refreshTokenService.generateToken(up);
//        return new LoginResponseModel(usersModel.getUsername(), accessToken, refreshToken);
//    }

    @Transactional
    public StatusMessage validateRegister(RegisterRequestModel model) {
        UsersModel usersModel = this.usersService.findById(model.getUsername());
        if (model.getUsername().equals(usersModel.getUsername())) {
            return StatusMessage.createInstanceFail("Username is duplicate");
        } else {
            return StatusMessage.createInstanceSuccess("Register successfully.");
        }
    }

    @Transactional
    public StatusMessage register(RegisterRequestModel model) {
        UsersModel user = RegisterRequestModel.toUser(model);
        MemberEntity memberEntity = MemberModel.toEntity(user.getMember());
        memberEntity.setStatus(EnableStatus.ENABLE.getValue());
        memberEntity = this.memberRepository.save(memberEntity);
        user.setMember(MemberModel.fromEntity(memberEntity));
        this.usersService.create(user);
        return StatusMessage.createInstanceSuccess("Register successfully.");
    }

    @Transactional
    public StatusMessage resetPassword(String username, String email) throws MessagingException {
        UsersModel usersModel = this.usersService.findById(username);
        if (email.equals(usersModel.getMember().getEmail())) {
            String tempPassword = RandomStringUtils.randomAlphanumeric(8);
            Calendar tempPasswordExpire = Calendar.getInstance();
            tempPasswordExpire.add(Calendar.HOUR_OF_DAY, 1);
            this.usersService.createTempPassword(usersModel.getUsername(), tempPassword, tempPasswordExpire.toInstant());
            String subject = "Reset password your myLearn";
            String content = "กรุณา Login your myLearn ด้วย password : " + tempPassword + " แล้วนำรหัสผ่านนี้ไปใช้ในการเปลี่ยนรหัสผ่านใหม่ \n รหัสผ่านที่นี้มีอายุ 1 ชั่วโมง";
            this.sendMailService.sendEmail(new String[]{usersModel.getMember().getEmail()}, new String[]{}, subject, content);
        } else {
            return StatusMessage.createInstanceFail("Email is incorrect");
        }
        return StatusMessage.createInstanceSuccess("Reset password successfully.");
    }
}
