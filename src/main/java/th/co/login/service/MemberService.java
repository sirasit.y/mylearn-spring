package th.co.login.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import th.co.login.entity.UsersEntity;
import th.co.login.exception.ResourceNotFoundException;
import th.co.login.model.MemberModel;
import th.co.login.model.ProfileModel;
import th.co.login.model.UsersRoleModel;
import th.co.login.model.message.StatusMessage;
import th.co.login.repository.MemberRepository;
import th.co.login.repository.UsersRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MemberService {
    @Autowired
    MemberRepository memberRepository;
    @Autowired
    UsersRepository usersRepository;

    @Transactional
    public StatusMessage create(MemberModel model) {
        this.memberRepository.save(MemberModel.toEntity(model));
        return StatusMessage.createInstanceSuccess("Data saved successfully.");
    }

    @Transactional
    public StatusMessage delete(MemberModel model) {
        this.memberRepository.findById(model.getMemberId())
                .ifPresent(e -> {
                    this.memberRepository.delete(e);
                });
        return StatusMessage.createInstanceSuccess("Data deleted successfully.");
    }

    @Transactional
    public List<MemberModel> findAllList() {
        return this.memberRepository.findAll(Sort.by(Sort.Direction.ASC, "thFirstname", "thLastname", "engFirstname", "engLastname"))
                .stream()
                .map(MemberModel::fromEntity)
                .collect(Collectors.toList());
    }

    @Transactional
    public Page<MemberModel> findPage(PageRequest page) {
        return this.memberRepository.findAll(page.withSort(Sort.by(Sort.Direction.ASC, "thFirstname", "thLastname", "engFirstname", "engLastname")))
                .map(MemberModel::fromEntity);
    }

    @Transactional
    public MemberModel findById(String memberId) {
        return this.memberRepository.findById(memberId)
                .map(MemberModel::fromEntity)
                .orElseThrow(() -> new ResourceNotFoundException("Data " + memberId + " not found"));
    }

//    @Transactional
//    public ProfileModel findByProfile(UProfile up) {
//        UsersEntity users = this.usersRepository.findById(up.getUsername())
//                .orElseThrow(() -> new ResourceNotFoundException("Data " + up.getUsername() + " not found"));
//        ProfileModel profile = ProfileModel.fromEntity(users.getMember());
//        profile.setRole(UsersRoleModel.fromEntity(users.getRole()));
//        return profile;
//    }
}
