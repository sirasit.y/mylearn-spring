package th.co.login.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import th.co.login.entity.UsersRoleEntity;

@Repository
public interface UsersRoleRepository extends JpaRepository<UsersRoleEntity, String> {
}
