package th.co.login.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import th.co.login.entity.UsersEntity;
import th.co.login.entity.UsersRoleEntity;

import java.time.Instant;
import java.util.Optional;
import java.util.stream.Stream;

@Repository
public interface UsersRepository extends JpaRepository<UsersEntity, String> {
    Stream<UsersEntity> findByRole(UsersRoleEntity role);

    Optional<UsersEntity> findByUsernameAndTempPasswordExpireAfter(String username, Instant tempPasswordExpire);
}
