package th.co.login.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import th.co.login.anotation.CommonArgument;
import th.co.login.model.MemberModel;
import th.co.login.model.ProfileModel;
import th.co.login.model.message.StatusMessage;
import th.co.login.resolver.CommonParameter;
import th.co.login.service.MemberService;

import java.util.List;

@RestController
@RequestMapping("/member")
public class MemberApi {
    @Autowired
    MemberService memberService;

    @PostMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage create(@RequestBody MemberModel request) {
        return this.memberService.create(request);
    }

    @DeleteMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage delete(@RequestBody MemberModel request) {
        return this.memberService.delete(request);
    }

    @GetMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public Page<MemberModel> findPage(@CommonArgument CommonParameter param) {
        return this.memberService.findPage(PageRequest.of(param.getPage(), param.getSize()));
    }

    @GetMapping("/lists")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public List<MemberModel> findAllList() {
        return this.memberService.findAllList();
    }

    @GetMapping("/{memberId}")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public MemberModel findById(@PathVariable(value = "memberId") String memberId) {
        return this.memberService.findById(memberId);
    }

//    @GetMapping("/profile")
//    @Parameter(name = "up", hidden = true)
//    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
//    public ProfileModel findByProfile(@UProfileArgument UProfile up) {
//        return this.memberService.findByProfile(up);
//    }
}
