package th.co.login.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import th.co.login.anotation.CommonArgument;
import th.co.login.model.UsersRoleModel;
import th.co.login.model.message.StatusMessage;
import th.co.login.resolver.CommonParameter;
import th.co.login.service.UsersRoleService;

import java.util.List;

@RestController
@RequestMapping("/users-role")
public class UsersRoleApi {
    @Autowired
    UsersRoleService roleService;

    @PostMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage create(@RequestBody UsersRoleModel request) {
        return this.roleService.create(request);
    }

    @DeleteMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage delete(@RequestBody UsersRoleModel request) {
        return this.roleService.delete(request);
    }

    @GetMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public Page<UsersRoleModel> findPage(@CommonArgument CommonParameter param) {
        return this.roleService.findPage(PageRequest.of(param.getPage(), param.getSize()));
    }

    @GetMapping("/lists")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public List<UsersRoleModel> findAllList() {
        return this.roleService.findAllList();
    }

    @GetMapping("/{roleId}")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public UsersRoleModel findById(@PathVariable(value = "roleId") String roleId) {
        return this.roleService.findById(roleId);
    }
}
