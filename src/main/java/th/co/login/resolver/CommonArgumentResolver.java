package th.co.login.resolver;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import th.co.login.anotation.CommonArgument;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

public class CommonArgumentResolver implements HandlerMethodArgumentResolver {
    private static final String EMPTY_STRING = "";

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterAnnotation(CommonArgument.class) != null;
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        HttpServletRequest request = (HttpServletRequest) webRequest.getNativeRequest();
        int page = Integer.parseInt(Optional.ofNullable(request.getParameter("page")).orElse("0"));
        int size = Integer.parseInt(Optional.ofNullable(request.getParameter("size")).orElse("10"));
        String memberId = Optional.ofNullable(request.getParameter("memberId")).orElse(EMPTY_STRING);
        return new CommonParameter(page, size, memberId);
    }
}
