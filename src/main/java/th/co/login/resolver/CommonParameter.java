package th.co.login.resolver;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CommonParameter {
    private int page;
    private int size;
    private String memberId;

//    public String getMemberId(UProfile up) {
//        return memberId.equals("") ? up.getEmployeeId() : this.memberId;
//    }
}
