package th.co.login.model;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import th.co.login.entity.MemberEntity;

@Data
public class MemberModel {
    private static Logger logger = LoggerFactory.getLogger(MemberModel.class);
    private String memberId;
    private String thFirstname;
    private String engFirstname;
    private String thLastname;
    private String engLastname;
    private String email;
    private String tel;
    private String picture;
    private int status;

    public String getThFullName() {
        return this.getThFirstname() + " " + this.getThLastname();
    }

    public String getEngFullName() {
        return this.getEngFirstname() + " " + this.getEngLastname();
    }

    public static MemberModel fromEntity(MemberEntity entity) {
        if (entity == null) {
            return null;
        }
        MemberModel model = new MemberModel();
        try {
            model.setMemberId(entity.getMemberId());
            model.setThFirstname(entity.getThFirstname());
            model.setEngFirstname(entity.getEngFirstname());
            model.setThLastname(entity.getThLastname());
            model.setEngLastname(entity.getEngLastname());
            model.setEmail(entity.getEmail());
            model.setTel(entity.getTel());
            model.setPicture(entity.getPicture());
            model.setStatus(entity.getStatus());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return model;
    }

    public static MemberEntity toEntity(MemberModel model) {
        if (model == null) {
            return null;
        }
        MemberEntity entity = new MemberEntity();
        try {
            entity.setMemberId(model.getMemberId());
            entity.setThFirstname(model.getThFirstname());
            entity.setEngFirstname(model.getEngFirstname());
            entity.setThLastname(model.getThLastname());
            entity.setEngLastname(model.getEngLastname());
            entity.setEmail(model.getEmail());
            entity.setTel(model.getTel());
            entity.setPicture(model.getPicture());
            entity.setStatus(model.getStatus());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return entity;
    }
}
