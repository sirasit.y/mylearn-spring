package th.co.login.model;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import th.co.login.entity.MemberEntity;

@Data
public class ProfileModel extends MemberModel {
    private static Logger logger = LoggerFactory.getLogger(ProfileModel.class);
    private UsersRoleModel role;

    public static ProfileModel fromEntity(MemberEntity entity) {
        if (entity == null) {
            return null;
        }
        ProfileModel model = new ProfileModel();
        try {
            model.setMemberId(entity.getMemberId());
            model.setThFirstname(entity.getThFirstname());
            model.setEngFirstname(entity.getEngFirstname());
            model.setThLastname(entity.getThLastname());
            model.setEngLastname(entity.getEngLastname());
            model.setEmail(entity.getEmail());
            model.setTel(entity.getTel());
            model.setPicture(entity.getPicture());
            model.setStatus(entity.getStatus());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return model;
    }
}
