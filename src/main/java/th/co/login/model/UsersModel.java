package th.co.login.model;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import th.co.login.entity.UsersEntity;

@Data
public class UsersModel {
    private static Logger logger = LoggerFactory.getLogger(UsersModel.class);
    private String username;
    private String password;
    private MemberModel member;
    private UsersRoleModel role;

    public static UsersModel fromEntity(UsersEntity entity) {
        if (entity == null) {
            return null;
        }
        UsersModel model = new UsersModel();
        try {
            model.setUsername(entity.getUsername());
            model.setPassword(entity.getPassword());
            model.setMember(MemberModel.fromEntity(entity.getMember()));
            model.setRole(UsersRoleModel.fromEntity(entity.getRole()));
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return model;
    }

    public static UsersEntity toEntity(UsersModel model) {
        if (model == null) {
            return null;
        }
        UsersEntity entity = new UsersEntity();
        try {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            entity.setUsername(model.getUsername());
            entity.setPassword(passwordEncoder.encode(model.getPassword()));
            entity.setMember(MemberModel.toEntity(model.getMember()));
            entity.setRole(UsersRoleModel.toEntity(model.getRole()));
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return entity;
    }
}
