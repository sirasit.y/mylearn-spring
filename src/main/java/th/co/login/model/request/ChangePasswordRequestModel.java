package th.co.login.model.request;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Data
public class ChangePasswordRequestModel {
    private static Logger logger = LoggerFactory.getLogger(ChangePasswordRequestModel.class);
    private String oldPassword;
    private String password;
}
