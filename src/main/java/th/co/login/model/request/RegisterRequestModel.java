package th.co.login.model.request;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import th.co.login.model.MemberModel;
import th.co.login.model.UsersModel;
import th.co.login.model.UsersRoleModel;

@Data
public class RegisterRequestModel {
    private static Logger logger = LoggerFactory.getLogger(RegisterRequestModel.class);
    private String username;
    private String password;
    private MemberModel member;

    public static UsersModel toUser(RegisterRequestModel model) {
        if (model == null) {
            return null;
        }
        UsersModel user = new UsersModel();
        try {
            user.setUsername(model.getUsername());
            user.setPassword(model.getPassword());
            user.setMember(model.getMember());
            UsersRoleModel role = new UsersRoleModel();
            role.setRoleId("member");
            user.setRole(role);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return user;
    }
}
