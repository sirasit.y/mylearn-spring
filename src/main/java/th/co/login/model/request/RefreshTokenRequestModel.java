package th.co.login.model.request;

import lombok.Data;

@Data
public class RefreshTokenRequestModel {
    String refreshToken;
}
