package th.co.login.model.request;

import lombok.Data;

@Data
public class LoginRequestModel {
    private String username;
    private String password;
}
