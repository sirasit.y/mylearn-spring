package th.co.login.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "member")
public class MemberEntity extends AbstractAuditingEntity {
    @Id
    @NotNull
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "uuid2")
    @Column(name = "member_id")
    private String memberId;
    @Column(name = "th_firstname", columnDefinition = "TEXT")
    private String thFirstname;
    @Column(name = "eng_firstname", columnDefinition = "TEXT")
    private String engFirstname;
    @Column(name = "th_lastname", columnDefinition = "TEXT")
    private String thLastname;
    @Column(name = "eng_lastname", columnDefinition = "TEXT")
    private String engLastname;
    @Column(name = "email")
    private String email;
    @Column(name = "tel")
    private String tel;
    @Column(name = "picture")
    private String picture;
    @Column(name = "status")
    private int status;
}
